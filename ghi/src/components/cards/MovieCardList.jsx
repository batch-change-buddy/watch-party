import React from "react";
import { useGetPopularQuery } from "../../store/moviesApi";
import MovieCard from "./MovieCard";

function MovieCardList() {
  const { data, error, isLoading } = useGetPopularQuery();

  if (isLoading) {
    return (
      <progress className="progress is-primary" />
    );
  }
  if (error) {
    console.log(error)
    return (
      <p>{error.error} --- {error.status}</p>
    )
  }

  return(
    <>
      <h1>
      Here is the page to display movie cards.
      </h1>
      <h3>{ error ? "ERROR!" : ""}</h3>
      <h3>{ data ? "Movies:" : "No movies found"}</h3>
      { data.map(movie => (
          <MovieCard movie={movie} />
      ))}
    </>
  );
}

export default MovieCardList;
